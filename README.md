# MCTS
This code is meant to ease the computation of a policy based on Monte Carlo Tree Search with standard UCT. The code has been developed at "Università degli studi di Verona", in Verona. Users can modify and/or distribute code free of charge, provided that this notice is retained.

# Folders
List of folders contained in MCTS:

Matrices: it contains the reward and transition matrix for SysAdmin problem;

# MCTS CODE FILES
List of code files:

mcts.py: the implementations of MCTS with standard UCT;
dynamic_computation.py: computes policy dynamically by simulating steps in real environment;
static_computation.py: computes policy for a set a states.


# Basic usage
-> Given a mcts object, the following instructions allow to get the stats:
mcts_policy = pib_MCTS.pi (The policy which is a matrix n_states x n_actions)
mcts_Q = pib_MCTS.q_values (Q-values which is a matrix n_states x n_actions)
mcts_V = pib_MCTS.v (V which is a vector of size equals to n_states)
ns = pib_MCTS.ns (the number of visit for each action node of the root)

# Authors
Alberto Castellini, Department of Computer Science, University of Verona, Italy, email: alberto.castellini@univr.it
Alessandro Farinelli, Department of Computer Science, University of Verona, Italy, email: alessandro.farinelli@univr.it
Federico Bianchi, Department of Computer Science, University of Verona, Italy, email: federico.bianchi@univr.it
Edoardo Zorzi, University of Verona, Italy, email: edoardo.zorzi@studenti.univr.it

# License
MCTS - Monte Carlo Tree Search
Copyright 2022 © Alberto Castellini, Alessandro Farinelli, Federico Bianchi, Edoardo Zorzi
This file is part of MCTS. MCTS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
MCTS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with MCTS.  If not, see http://www.gnu.org/licenses/.
Please, report suggestions/comments/bugs to
alberto.castellini@univr.it, alessandro.farinelli@univr.it, federico.bianchi@univr.it, edoardo.zorzi@studenti.univr.it
